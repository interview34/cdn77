# CDN77

### Install ansible and community.docker module and sshpass

Run command ``apt-get install ansible``
Run command ``ansible-galaxy collection install community.docker``
Run command ``apt-get install sshpass``

### Edit ansible hosts on path ``/etc/ansible/hosts``

Add -

```
[web]
192.168.0.2

[proxy]
192.168.0.3

[with_output]
192.168.0.2

#[etcd]
#192.168.0.4
#192.168.0.5
#192.168.0.6
#192.168.0.7
#192.168.0.8

[all:vars]
ansible_user=root
ansible_password=test
```

### Testing environment
```
NAME="Ubuntu"
VERSION="20.04.3 LTS (Focal Fossa)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 20.04.3 LTS"
VERSION_ID="20.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=focal
UBUNTU_CODENAME=focal
```

### Grafana
Collecting data from prometheus locate on ``http://46.101.130.133:3000``

### To sync data from localhost to remote host

Run ``rsync -avzh ../cdn77.nosync root@<ip>:/root/``

### Docker images
Grafana - https://hub.docker.com/r/grafana/grafana
Ubuntu with prometheus - https://hub.docker.com/r/ubuntu/prometheus

#### Sources
- https://devopscube.com/setup-etcd-cluster-linux/
