## Exercise

1. install proxmox/docker/lxc/… & prometheus at your machine, spin up 10 tiny VMs/containers - **DONE**
2. use one as nginx origin, serving any static files you like - **DONE**
3. use one as nginx caching reverse proxy, using your origin as an upstream - **DONE**
4. use another 5 for etcd, fine tune the settings to your liking - **DONE**
5. use the last 3 for client stations, debugging, etc
6. write a simple “Hello, world” script in any language you like - **DONE**
7. run that script as a supervise service at all your client VMs - **DONE**
8. make only one instance of the service actually produce an output - **DONE**
9. use etcd for solving the master election problem of your service
10. use prometheus to collect metrics from service/blackbox/node exporters from each VM - **DONE**
11. use nft to set reasonable firewall rules for all the services - **DONE** (Ne cele, nastavil jsem pouze pro nginx origin.)
12. fine tune any additional settings you find along the way

Send the link of your GitHub repo containing Ansible playbooks for automated setup of this infrastructure to game@cdn77.com.
