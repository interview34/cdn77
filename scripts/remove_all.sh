#!/bin/bash

# Go to dir of the script
DIR=`readlink -e $0`
DIR=`dirname $DIR`

cd $DIR

sh remove_all_containers.sh
sh remove_all_images.sh
