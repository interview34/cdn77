#!/bin/bash

# Go to dir of the script
DIR=`readlink -e $0`
DIR=`dirname $DIR`

cd $DIR/..

# Run ansible playbook
ansible-playbook playbooks/playbook.yml --ask-become-pass
